<?php

namespace Source\Reconciliation;

use App\Jobs\InvalidTransactions;
use Source\Reconciliation\Exceptions\InvalidResourceException;
use Source\Reconciliation\Validators\MatchFileValidator;

/**
 * Class MatchFilesService
 * @package Source\Reconciliation
 */
class MatchFilesService
{
    /** @var MatchFileValidator $matchFileValidator */
    private $matchFileValidator;

    /**
     * MatchFilesService constructor.
     * @param MatchFileValidator $matchFileValidator
     */
    public function __construct(MatchFileValidator $matchFileValidator)
    {
        $this->matchFileValidator = $matchFileValidator;
    }

    /**
     * Php dosent have a resource type, check if the parameter is a resource
     *
     * @param resource $data
     */
    private function checkResourceType($data)
    {
        $this->matchFileValidator->validate($data);
    }

    /**
     * Parse files and build a hash based on the csv line, count the number of the same hash
     * if the hash appears twice it means we have a match, if the hash appears more then twice ignore it
     * if the hash appears only once it means we have an unmatched transaction
     *
     * @param resource $tutuka
     * @param resource $client
     * @return array
     */
    public function tryMatchFiles($tutuka, $client): array
    {
        try {
            $this->checkResourceType($tutuka);
            $this->checkResourceType($client);
        } catch (InvalidResourceException $exception) {
            return [];
        }

        $count = [];
        $nr = 0;
        $linesNumberClient = 0;
        $linesNumberTutuka = 0;

        // ToDo move this procedural code into an object
        // note we are assuming that the data is correct and we dont need to do any kind of data modification
        // Calculate the number of hashes so we can count the matching transactions
        while (($line = fgets($client)) !== false) {
            @$count[sha1($line)]++;
            $linesNumberClient++;
        }

        while (($line = fgets($tutuka)) !== false) {
            $hash = sha1($line);
            @$count[$hash]++;
            $linesNumberTutuka++;

            if ($count[$hash] === 2) {
                $nr++;
            }
        }

        // ToDo move this into am unmatchedStatement object
        // get the unmatched transactions and send them to the suggestion service
        $unmatchedStatements = $this->checkUnmatchedRecords($tutuka, $client, $count);
        $this->flushUnmatchedStatements($unmatchedStatements['client'], $unmatchedStatements['tutuka']);

        return [
            'match'  => $nr,
            'client' => $linesNumberClient,
            'tutuka' => $linesNumberTutuka,
        ];
    }

    /**
     * Get array of unmatched transactions
     *
     * @param resource $tutukaFile
     * @param resource $clientFile
     * @param array $countVector
     * @return array
     */
    private function checkUnmatchedRecords($tutukaFile, $clientFile, $countVector): array
    {
        rewind($clientFile);
        rewind($tutukaFile);

        $unmatchedClientTransactions = [];
        while (($line = fgets($clientFile)) !== false) {
            if ($countVector[sha1($line)] < 2) {
                $unmatchedClientTransactions[] = $line;
            }
        }

        $unmatchedTutukaTransactions = [];
        while (($line = fgets($tutukaFile)) !== false) {
            if ($countVector[sha1($line)] < 2) {
                $unmatchedTutukaTransactions[] = $line;
            }
        }

        return [
            'client' => $unmatchedClientTransactions,
            'tutuka' => $unmatchedTutukaTransactions,
        ];
    }

    /**
     * Send unmatched transactions to the second service via queues
     *
     * @param array $clientStatements
     * @param array $tutukaStatements
     */
    private function flushUnmatchedStatements($clientStatements, $tutukaStatements)
    {
        InvalidTransactions::dispatch(json_encode([
            'client' => $clientStatements,
            'tutuka' => $tutukaStatements,
        ]));
    }

}