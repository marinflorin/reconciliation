<?php

namespace Source\Reconciliation\Exceptions;

/**
 * Class InvalidFileFormatException
 * @package Source\Reconciliation\Exceptions
 */
class InvalidFileFormatException extends \Exception
{

}