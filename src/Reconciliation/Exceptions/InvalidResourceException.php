<?php

namespace Source\Reconciliation\Exceptions;

use Throwable;

/**
 * Class InvalidResourceException
 * @package Source\Reconciliation\Exceptions
 */
class InvalidResourceException extends \Exception
{

    public function __construct($variable, $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf(
            'Argument must be a valid resource type. %s given.',
            gettype($variable)
        ), $code, $previous);
    }
}