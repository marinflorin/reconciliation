<?php

namespace Source\Reconciliation\Contracts\Validators;

/**
 * Interface ValidatorInterface
 * @package Source\Reconciliation\Contracts\Validators
 */
interface ValidatorInterface
{
    public function validate($variable);
}