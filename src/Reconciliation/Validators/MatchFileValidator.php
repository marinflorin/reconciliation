<?php

namespace Source\Reconciliation\Validators;

use Source\Reconciliation\Contracts\Validators\ValidatorInterface;
use Source\Reconciliation\Exceptions\InvalidResourceException;

/**
 * Class MatchFileValidator
 * @package Source\Reconciliation\Validators
 */
class MatchFileValidator implements ValidatorInterface
{
    /**
     * @param $variable
     * @throws InvalidResourceException
     */
    public function validate($variable)
    {
        if (false === is_resource($variable)) {
            throw new InvalidResourceException($variable);
        }
    }
}
