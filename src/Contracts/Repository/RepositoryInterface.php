<?php

namespace Source\Contracts\Repository;

/**
 * Interface RepositoryInterface
 */
interface RepositoryInterface
{
    public function findAll();

    public function find($id);

    public function delete($id);
}