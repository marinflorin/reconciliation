<?php

namespace Source\Report;

use Source\Contracts\Repository\AbstractRepository;
use Source\Contracts\Repository\RepositoryInterface;

/**
 * Class ReportRepository
 * @package Source\Report
 * @property Report model
 */
class ReportRepository extends AbstractRepository implements RepositoryInterface
{
    /**
     * ReportRepository constructor.
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->model = $report;
    }

    /**
     * @param string $name
     * @param string $data
     * @return Report
     */
    public function create(string $name, string $data): Report
    {
        $report = new $this->model();
        $report->name = $name;
        $report->json = $data;

        $report->save();

        return $report;
    }
}
