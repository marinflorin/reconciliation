@extends('layouts.main')

@section('content')

    <div class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Select files to compare</div>
            <div class="panel-body">

                <form class="form-horizontal" action="{{ route('reconciliation.match') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Select file 1</label>
                        <div class="col-sm-8">
                            <input type="file" name="file[]" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Select file 2</label>
                        <div class="col-sm-8">
                            <input type="file" name="file[]" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-success">Compare</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>

    @if(isset($files))
        <div class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
            <div class="panel panel-primary">
                <div class="panel-heading">Comparison results</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-1" style="border: 3px solid;">
                            <h3>{{ $files[0] }}</h3>
                            <h4>Total Records: {{ $tutuka }} </h4>
                            <h4>Matching Records: {{ $match }} </h4>
                            <h4>Unmatched Records: {{ $tutuka - $match }} </h4>
                        </div>
                        <div class="col-md-4 col-md-offset-2" style="border: 3px solid;">
                            <h3>{{ $files[1] }}</h3>

                            <h4>Total Records: {{ $client }} </h4>
                            <h4>Matching Records: {{ $match }} </h4>
                            <h4>Unmatched Records: {{ $client - $match }} </h4>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="row text-center">
                        <button onclick="getReport()" class="text-center btn btn-success">Unmatched Report</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1" style="margin-top: 50px;">
            <div class="panel panel-primary">
                <div class="panel-heading">Unmatched reports</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th colspan="3">{{ $files[1] ?? '' }}</th>
                                <th colspan="3">{{ $files[0] ?? '' }}</th>
                            </tr>
                            <tr>
                                <th>Reference</th>
                                <th>Date</th>
                                <th>Merchant</th>
                                <th>Amount</th>
                                <th>Wallet Reference</th>
                                <th>Reference</th>
                                <th>Date</th>
                                <th>Merchant</th>
                                <th>Amount</th>
                                <th>Wallet Reference</th>
                            </tr>
                            </thead>
                            <tbody id="reportBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('after_scripts')
    <script>
        function getReport() {
            jQuery.ajax("{{ route('reconciliation.report') }}").done(function (data) {
                jQuery('#reportBody').html(data);
            });
        }
    </script>
@endsection