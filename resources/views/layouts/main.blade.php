<!doctype html>
<html lang="{{ app()->getLocale() }}">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title> Reconciliation </title>

    @yield('before_styles')

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}">

    @yield('after_styles')

</head>
<body>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

<!-- Main content -->
    <section class="content">

        @yield('content')

    </section>
    <!-- /.content -->
</div>

@yield('before_scripts')

<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>

@yield('after_scripts')

</body>
</html>
