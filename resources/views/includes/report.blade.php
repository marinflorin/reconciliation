@foreach($client as $key => $bankStatement)
    @if(null != $bankStatement['dirty'])
        <tr class="success">
            <td>{{ $key }}</td>
            <td>{{ $bankStatement['place']['date'] }}</td>
            <td>{{ $bankStatement['place']['merchant']['name'] }}</td>
            <td>{{ $bankStatement['price']['amount'] }} <br/> {{ $bankStatement['price']['description'] }}</td>
            <td title="{{ $bankStatement['meta']['walletReference'] }}">{{ substr($bankStatement['meta']['walletReference'],0,5 )}}</td>

            @if(isset($tutuka[$key]))
                <td>{{ $key }}</td>
                <td>{{ $tutuka[$key]['place']['date'] }}</td>
                <td>{{ $tutuka[$key]['place']['merchant']['name'] }}</td>
                <td>{{ $tutuka[$key]['price']['amount'] }} <br/> {{ $tutuka[$key]['price']['description'] }}</td>
                <td title="{{ $tutuka[$key]['meta']['walletReference'] }}">{{ substr($tutuka[$key]['meta']['walletReference'],0,5 )}}</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        </tr>
        <tr class="danger">
            <td>{{ $key }}</td>
            <td>{{ $bankStatement['dirty']['place']['date'] }}</td>
            <td>{{ $bankStatement['dirty']['place']['merchant']['name'] }}</td>
            <td>{{ $bankStatement['dirty']['price']['amount'] }} <br/> {{ $bankStatement['dirty']['price']['description'] }}</td>
            <td title="{{ $bankStatement['dirty']['meta']['walletReference'] }}">{{ substr($bankStatement['dirty']['meta']['walletReference'],0,5 )}}</td>

            @if(isset($tutuka[$key]))
                <td>{{ $key }}</td>
                <td>{{ $tutuka[$key]['dirty']['place']['date'] }}</td>
                <td>{{ $tutuka[$key]['dirty']['place']['merchant']['name'] }}</td>
                <td>{{ $tutuka[$key]['dirty']['price']['amount'] }} <br/> {{ $tutuka[$key]['price']['description'] }}</td>
                <td title="{{ $tutuka[$key]['dirty']['meta']['walletReference'] }}">{{ substr($tutuka[$key]['dirty']['meta']['walletReference'],0,5 )}}</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        </tr>
    @else
        <tr>
            <td>{{ $key }}</td>
            <td>{{ $bankStatement['place']['date'] }}</td>
            <td>{{ $bankStatement['place']['merchant']['name'] }}</td>
            <td>{{ $bankStatement['price']['amount'] }} <br/> {{ $bankStatement['price']['description'] }}</td>
            <td title="{{ $bankStatement['meta']['walletReference'] }}">{{ substr($bankStatement['meta']['walletReference'],0,5 )}}</td>

            @if(isset($tutuka[$key]))
                <td>{{ $key }}</td>
                <td>{{ $tutuka[$key]['place']['date'] }}</td>
                <td>{{ $tutuka[$key]['place']['merchant']['name'] }}</td>
                <td>{{ $tutuka[$key]['price']['amount'] }} <br/> {{ $tutuka[$key]['price']['description'] }}</td>
                <td title="{{ $tutuka[$key]['meta']['walletReference'] }}">{{ substr($tutuka[$key]['meta']['walletReference'],0,5 )}}</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        </tr>
    @endif

@endforeach

@foreach($tutuka as $key => $bankStatement)
    @if(!isset($client[$key]))
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $key }}</td>
            <td>{{ $bankStatement['place']['date'] }}</td>
            <td>{{ $bankStatement['place']['merchant']['name'] }}</td>
            <td>{{ $bankStatement['price']['amount'] }} <br/> {{ $bankStatement['price']['description'] }}</td>
            <td title="{{ $bankStatement['meta']['walletReference'] }}">{{ substr($bankStatement['meta']['walletReference'],0,5 )}}</td>

        </tr>
        @endif
        @endforeach
        </tbody>