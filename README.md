# Match Transaction Service #

This service is responsible for design and getting the number of matching transactions ASAP, unmatched transactions will be sent to another service and do the business logic in background.

####Advantages of this architecture:
1. Replaceability, if we decide to use a 3-rd party, or another language which is faster or any other business logic we can replace anytime the second service.
2. Performance and cost, if we have too many requests on an important event, we can cut the second service and use only the first service which does only light operations, this way we cover ~80%+ of transactions almost immediatly
3. Paralelism, we can have multiple teams working in the same time, first team can work on first service while the second team can work on the second service.
4. Independent caching systems and frameworks
5. Independent deployment
6. Etc

### Communication between services ###

The communication between services is done with queues. This allows us a background processing of large files and also data reliability. 

### Improvements ###

1. Split Presentation, maybe use something like Angular,React,etc and integrate with Api calls
2. Add validators in controller for files
3. Add alerts
4. Replace arrays with real objects
5. Check the dirty values and highlight what can be corrected
6. Fix report to have an unique identifier by files name and timestamp