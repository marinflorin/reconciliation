<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Source\Reconciliation\MatchFilesService;
use Source\Report\ReportRepository;

/**
 * Class ReconciliationController
 * @package App\Http\Controllers
 */
class ReconciliationController extends Controller
{
    /** @var MatchFilesService $matchFilesService */
    private $matchFilesService;

    /** @var ReportRepository $reportRepository */
    private $reportRepository;

    /**
     * ReconciliationController constructor.
     * @param MatchFilesService $matchFilesService
     * @param ReportRepository $reportRepository
     */
    public function __construct(MatchFilesService $matchFilesService, ReportRepository $reportRepository)
    {
        $this->matchFilesService = $matchFilesService;
        $this->reportRepository = $reportRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function matchFiles(Request $request)
    {
        if (count($_FILES) != 1) {
            dd('please upload the 2 csv files');
        }

        /** @var UploadedFile[] $files */
        $files = $request->allFiles()['file'];
        $client = fopen($files[0]->getPathName(), 'r');
        $tutuka = fopen($files[1]->getPathName(), 'r');

        // Get files name and number of matching transactions
        $response = $this->matchFilesService->tryMatchFiles($client, $tutuka);
        $response['files'][] = $files[0]->getClientOriginalName();
        $response['files'][] = $files[1]->getClientOriginalName();

        // Display the result inside the view
        return view('welcome', $response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReport()
    {
        try {
            $report = $this->reportRepository->findAll()->last()->json;
        } catch (ModelNotFoundException $exception) {
            return response()->json([], 204);
        } catch (\Exception $exception) {
            return response()->json([], 400);
        }

        return view('includes.report', json_decode($report, true));
    }
}
