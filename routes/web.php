<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('reconciliation', 'ReconciliationController@matchFiles')->name('reconciliation.match');
Route::get('reconciliation/report', 'ReconciliationController@getReport')->name('reconciliation.report');
